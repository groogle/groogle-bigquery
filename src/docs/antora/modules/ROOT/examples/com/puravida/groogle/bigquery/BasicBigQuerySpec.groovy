package com.puravida.groogle.bigquery

import com.google.api.services.bigquery.Bigquery
import com.google.api.services.bigquery.BigqueryScopes
import com.google.cloud.bigquery.BigQuery
import com.google.cloud.bigquery.FieldValueList
import com.google.cloud.bigquery.Job
import com.google.cloud.bigquery.JobId
import com.google.cloud.bigquery.JobInfo
import com.google.cloud.bigquery.QueryJobConfiguration
import com.google.cloud.bigquery.QueryResponse
import com.google.cloud.bigquery.TableResult
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import com.puravida.groogle.bigquery.impl.GroovyBigQueryService

import spock.lang.Shared
import spock.lang.Specification

class BasicBigQuerySpec extends Specification{

    @Shared
    Groogle groogle

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withServiceCredentials {}
            service(BigQueryBuilder.build(), BigQueryService)   //<1>
        }
        //end::register[]
    }



    def "query stackoverflow"(){

        given: "a biquery"
        BigQueryService service = groogle.service(BigQueryService)

        when:

        List<Map> rows = service.rows("""SELECT 
                CONCAT('https://stackoverflow.com/questions/', CAST(id as STRING)) as url, view_count 
                FROM `bigquery-public-data.stackoverflow.posts_questions` 
                WHERE tags like '%groovy%' 
                ORDER BY favorite_count DESC LIMIT 10""")

        rows.each{
            println it
        }

        then:
        rows.size()
    }

    def "query all trolley"(){

        given: "a biquery"

        BigQueryService service = groogle.service(BigQueryService)

        when:

        List<Map> rows = service.rows("""
            SELECT COUNT(*) FROM
            `bigquery-public-data.open_images.labels` a
            INNER JOIN
              `bigquery-public-data.open_images.images` b
            ON
              a.image_id = b.image_id
            WHERE
              a.label_name='/m/0f6pl'
              AND a.confidence > 0.5;
            """)

        rows.each{
            println it
        }

        then:
        rows.size()
    }

    def "query each trolley"(){

        given: "a biquery"

        BigQueryService service = groogle.service(BigQueryService)

        when:

        //[image_id:028c742176f8eb7c, source:machine, label_name:/m/0f6pl, confidence:0.6]
        int count = service.eachRow("""
            SELECT b.* FROM
                `bigquery-public-data.open_images.labels` a
            INNER JOIN
                `bigquery-public-data.open_images.images` b
            ON
              a.image_id = b.image_id
            INNER JOIN
                `bigquery-public-data.open_images.dict` c
            ON
              a.label_name = c.label_name
            WHERE
              c.label_display_name LIKE '%bus%'
              AND a.confidence > 0.5
            LIMIT 10         
            """, { row->

            println row
        })

        then:
        count
    }


    def "query prestamos"(){

        given: "a biquery"

        BigQueryService service = groogle.service(BigQueryService)

        when:

        List<Map> rows = service.rows("SELECT COUNT(*) prestamos FROM `m30-bot.test.prestamos` ")

        rows.each{
            println it
        }

        then:
        rows.size()
    }

    def "list top prestamos"(){

        given: "a biquery"

        BigQueryService service = groogle.service(BigQueryService)

        when:

        int count = service.eachRow("""SELECT tititu, count(*) prestado
                    FROM `m30-bot.test.prestamos`
                    where prcocp='PRL'
                    group by 1 order by 2 desc limit 30
                    """, {
            println "$it.tititu ha sido prestado $it.prestado veces"
        })

        then:
        count == 30
    }


}
