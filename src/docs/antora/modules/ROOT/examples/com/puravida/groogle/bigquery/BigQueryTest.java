package com.puravida.groogle.bigquery;

import com.puravida.groogle.Groogle;
import com.puravida.groogle.GroogleBuilder;
import org.junit.Before;
import org.junit.Test;

public class BigQueryTest {

    Groogle groogle;

    @Before
    public void setUp() {
        //tag::register[]
        groogle = GroogleBuilder.build( build ->{
            build
                    .withServiceCredentials( credentials ->{

                    })
                    .service(BigQueryBuilder.build(), BigQueryService.class);   //<1>
        });
        //end::register[]
    }

    @Test
    public void listPrestamos(){
        //tag::simpleQuery[]
        BigQueryService service = groogle.service(BigQueryService.class);

        String query = "SELECT tititu, count(*) prestado FROM `m30-bot.test.prestamos` "+
                " where prcocp='PRL' "+
                " group by 1 order by 2 desc limit 3000";

        int count = service.eachRow(query, row ->{
            System.out.println(row.get("tititu")+" ha sido prestado "+row.get("prestado")+" veces");
        });
        assert count!=0;
        //end::simpleQuery[]
    }

}
